from twisted.internet.protocol import Protocol, Factory
from zope.interface.declarations import implements
from twisted.internet.interfaces import ITransport, IStreamServerEndpoint
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING
from playground.network.common.Protocol import StackingTransport,\
    StackingProtocolMixin, StackingFactoryMixin

class RIPMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "RIPStack.RIPMessage"
    MESSAGE_VERSION = "1.0"
    
    BODY = [("data",STRING)]    
    
class MyTransport(StackingTransport):

	def write(self, data):
		Packet = RIPMessage()
		Packet.data = data
		self.lowerTransport().write(Packet.__serialize__())
		
class RIPProtocol(StackingProtocolMixin, Protocol):

	def __init__(self):
		self.buffer=""
		
	def connectionMade(self):
		HT = MyTransport(self.transport)
		self.makeHigherConnection(HT)
		
	def dataReceived(self, data):
	
		try:
			gotData, bytesUsed = RIPMessage.Deserialize(data)
			self.buffer = self.buffer[bytesUsed:]
		except Exception, e:
			print "Oops! Error: ", e
			return
		sendingData = gotData.data
		self.higherProtocol() and self.higherProtocol().dataReceived(sendingData)
		self.buffer and self.dataReceived("")


class RIPFactory(Factory, StackingFactoryMixin):
	def buildProtocol(self,addr):
		return RIPProtocol()
	
ConnectFactory = RIPFactory
ListenFactory = RIPFactory 
        
