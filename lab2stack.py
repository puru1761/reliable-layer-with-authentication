from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor, task
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.message.StandardMessageSpecifiers import STRING, UINT4, BOOL1, DEFAULT_VALUE, LIST, OPTIONAL, UINT1
from playground.network.common.Protocol import StackingTransport, \
    StackingProtocolMixin, StackingFactoryMixin
from playground.network.common.Protocol import MessageStorage
from playground.network.common.statemachine import StateMachine
#Crypto Imports
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from playground.crypto import X509Certificate
from Crypto.Hash import SHA256
from playground.crypto import PkiPlaygroundAddressPair
from random import randint
import CertFactory
import os

SentPackets = []
RecvdPackets = []



#The Control Block
class ControlBlock():

    def __init__(self):
        self.NextSequenceNumber=0
        self.NextAcknowledgementNumber=0
        self.Nonce = 0
        self.window = 1
        self.MSS = 4096
        self.bcert = ""
        self.scert= ""
        self.sessionID = ""


t = {}
test=[]

#The Message Definition
class RIPMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "RIP.RIPMessageID"

    MESSAGE_VERSION = "1.0"

    BODY = [("sequence_number", UINT4),
            ("acknowledgement_number", UINT4, OPTIONAL),
            ("signature", STRING, DEFAULT_VALUE("")),
            ("certificate", LIST(STRING), OPTIONAL),
            ("sessionID", STRING, DEFAULT_VALUE("")),
            ("acknowledgement_flag", BOOL1, DEFAULT_VALUE(False)),
            ("close_flag", BOOL1, DEFAULT_VALUE(False)),
            ("sequence_number_notification_flag", BOOL1, DEFAULT_VALUE(False)),
            ("reset_flag", BOOL1, DEFAULT_VALUE(False)),
            ("data", STRING, DEFAULT_VALUE("")),
            ("OPTIONS", LIST(STRING), OPTIONAL)
            ]

#The Transport
class MyHigherTransport(StackingTransport):

    def __init__(self, lowerTransport, protocol, cb):
        self.cb = protocol.cb
        self.protocol = protocol
        StackingTransport.__init__(self, lowerTransport)
        self.k = 0

    def write(self,data):
        i=0
        
        p = self.cb.MSS
        self.cb.window = len(data)/self.cb.MSS
        if len(data)%self.cb.MSS >0:
            self.cb.window = self.cb.window+1
        for iterations in range(0,self.cb.window):
            sdata = data[i:i+p]
            Packet = RIPMessage()
            Packet.sequence_number = self.cb.NextSequenceNumber
            Packet.acknowledgement_number = self.cb.NextAcknowledgementNumber
            Packet.sessionID = self.cb.sessionID
            Packet.data = sdata
            self.cb.NextSequenceNumber = self.cb.NextSequenceNumber + len(sdata)
            Packet.signature = self.signer(Packet)
            test.append((Packet.sequence_number, Packet))
            SentPackets.append((Packet.sequence_number, Packet))
            self.lowerTransport().write(Packet.__serialize__())
            i=i+p
        task.deferLater(reactor, 4, self.protocol.Retransmission, test, 0)
        del test[:]

    def loseConnection(self):
        #print "Closing Connection.."
        FIN = RIPMessage()
        FIN.sequence_number = self.cb.NextSequenceNumber
        FIN.acknowledgement_number = self.cb.NextAcknowledgementNumber
        FIN.sessionID = self.cb.sessionID
        FIN.close_flag = True
        FIN.signature = self.signer(FIN)
        self.protocol.fsm.signal("SEND_FIN", "")
        self.lowerTransport().write(FIN.__serialize__())


    def signer(self, data):
        c = CertFactory.getPrivateKeyForAddr(self.lowerTransport().getHost())
        rsaKey = RSA.importKey(c)
        rsaSigner = PKCS1_v1_5.new(rsaKey)
        hasher = SHA256.new()
        hasher.update(data.__serialize__())
        sigbytes = rsaSigner.sign(hasher)
        return sigbytes

    def getHost(self):
        certs = CertFactory.getCertsForAddr(self.lowerTransport().getHost().host)
        Scert = certs[1]
        Bcert = certs[0]
        cert1 = X509Certificate.loadPEM(Scert)
        cert2 = X509Certificate.loadPEM(Bcert)
        certChain = [cert2,cert1]
        key1 = RSA.importKey(CertFactory.getPrivateKeyForAddr(self.lowerTransport().getHost().host))
        return PkiPlaygroundAddressPair.ConvertHostPair(self.lowerTransport().getHost(), certChain, key1)

    def getPeer(self):
        Scert = self.protocol.cb.scert
        Bcert = self.protocol.cb.bcert
        cert1 = X509Certificate.loadPEM(Scert)
        cert2 = X509Certificate.loadPEM(Bcert)
        certChain = [cert2,cert1]
        return PkiPlaygroundAddressPair.ConvertPeerPair(self.lowerTransport().getPeer(), certChain)

InitCSeqNum = 100
InitSSeqNum = 300
#The Protocol
class RIPProtocol(StackingProtocolMixin, Protocol):

    def __init__(self):
        self.buffer = ""
        self.storage = MessageStorage()
        self.fsm = StateMachine("RIPStateMachine")
        self.fsm.addState("Closed", ("RESET", "Listening"), onEnter=self.finAck)
        self.fsm.addState("FIN-RECVD",("RESET","Listening"), ("FINACK", "Closed"), ("SEND_FIN", "FIN-SENT"), onEnter=self.sendFinAck)
        self.fsm.addState("Listening", ("SYN-RCVD", "SYN_RECVD"), onEnter=self.Listening)
        self.fsm.addState("Open", ("SYN-SENT", "SYN_SENT"))
        self.fsm.addState("SYN_SENT", ("SYNACK", "Established"), ("RESET", "Open"), onEnter=self.sendSyn, onExit=self.sendAck)
        self.fsm.addState("SYN_RECVD", ("ACK", "Established"), ("RESET", "Listening"), onEnter=self.sendSynAck)
        self.fsm.addState("Established", ("FIN", "FIN-RECVD"), ("ACK1", "Established"), ("SEND_FIN", "FIN-SENT"),("RESET", "Closed"), onEnter=self.Established)
        self.fsm.addState("FIN-SENT", ("FINACK", "Closed"),("ACK1", "FIN-SENT"), ("FIN", "FIN-RECVD"), onEnter=self.waitFinack)
        self.cb = ControlBlock()



    def connectionMade(self):
        self.HigherTransport = MyHigherTransport(self.transport, self, self.cb)
        self.segmentnumber=0
        if (self.factory.state=="client"):
            self.fsm.start("Open")
            self.fsm.signal("SYN-SENT", "")
        else:
            self.fsm.start("Listening")


    def dataReceived(self, data):

        self.storage.update(data)
        for msg in self.storage.iterateMessages():
            try:
                gotData = msg
            except Exception, e:
                print "Oops! Error: ", e
                return
            if (gotData.sequence_number_notification_flag and not gotData.acknowledgement_flag and self.fsm.currentState()=="Listening"):
                self.fsm.signal("SYN-RCVD", gotData)
            elif(gotData.sequence_number_notification_flag and gotData.acknowledgement_flag and self.fsm.currentState()=="SYN_SENT"):
                self.fsm.signal("SYNACK", gotData)
            elif (gotData.acknowledgement_flag and self.fsm.currentState()=="SYN_RECVD"):
                if (self.verifier(self.cb.scert, self.cb.bcert, gotData.certificate[0])):
                    self.fsm.signal("ACK", gotData)
                else:
                    self.fsm.signal("RESET","")
            elif(gotData.data!="" and self.fsm.currentState()=="Established"):
                self.Transmission(gotData)
            elif(gotData.data=="" and self.fsm.currentState()=="Established" and not gotData.close_flag and gotData.acknowledgement_flag):
                self.fsm.signal("ACK1", gotData)
            elif(gotData.close_flag and (self.fsm.currentState()=="Established" or self.fsm.currentState()=="FIN-SENT") and not gotData.acknowledgement_flag):
                self.fsm.signal("FIN", gotData)
            elif(gotData.close_flag and gotData.acknowledgement_flag):
                self.fsm.signal("FINACK", gotData)
            elif (gotData.sequence_number_notification_flag and self.fsm.currentState()=="Established"):
                self.fsm.signal("RESET","")
            elif ((self.fsm.currentState()=="FIN-SENT" or self.fsm.currentState()=="FIN-RECVD") and gotData.acknowledgement_flag and not gotData.close_flag):
                self.Established("ACK1", gotData)
            elif (self.fsm.currentState()=="Established" and gotData.sequence_number_notification_flag):
                if self.IntegrityCheck(gotData, gotData.certificate[1]):
                    self.higherProtocol().connectionLost(self.HigherTransport)

    def IntegrityCheck(self, data, cert):
        cert2 = X509Certificate.loadPEM(cert)
        peerPublicKeyBlob = cert2.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(peerPublicKeyBlob)
        rsaVerifier = PKCS1_v1_5.new(peerPublicKey)
        sig = data.signature
        data.signature = ""
        hasher = SHA256.new()
        hasher.update(data.__serialize__())
        result = rsaVerifier.verify(hasher, sig)
        data.signature = sig
        return result

    def verifier(self, Scert, Bcert, data):
        cert1 = X509Certificate.loadPEM(Scert)
        cert2 = X509Certificate.loadPEM(Bcert)

        c = CertFactory.getRootCert()
        rootCert = X509Certificate.loadPEM(c)
        if (cert1.getIssuer() != rootCert.getSubject()):
            return False
        if(cert2.getIssuer() != cert1.getSubject()):
            return False
        if(cert2.getSubject()["commonName"]!= str(self.transport.getPeer()).split(":")[0]):
            return False

        rootPkBytes = rootCert.getPublicKeyBlob()
        rootPublicKey = RSA.importKey(rootPkBytes)
        rsaVerifier = PKCS1_v1_5.new(rootPublicKey)
        bytesToVerify = cert1.getPemEncodedCertWithoutSignatureBlob()
        hasher = SHA256.new()
        hasher.update(bytesToVerify)
        if not rsaVerifier.verify(hasher, cert1.getSignatureBlob()):
            return False

        rootPkBytes = cert1.getPublicKeyBlob()
        rootPublicKey = RSA.importKey(rootPkBytes)
        rsaVerifier = PKCS1_v1_5.new(rootPublicKey)
        bytesToVerify = cert2.getPemEncodedCertWithoutSignatureBlob()
        hasher = SHA256.new()
        hasher.update(bytesToVerify)
        if not rsaVerifier.verify(hasher, cert2.getSignatureBlob()):
            return False

        return True
        
    def intToNonce(self, i):
        h = hex(i)
        h = h[2:]
        if h[-1] == 'L':
            h = h[:-1] 
        return h

    def PacketSigner(self, data):
        c = CertFactory.getPrivateKeyForAddr(self.transport.getHost().host)
        rsaKey = RSA.importKey(c)
        rsaSigner = PKCS1_v1_5.new(rsaKey)
        hasher = SHA256.new()
        hasher.update(data.__serialize__())
        sigbytes = rsaSigner.sign(hasher)
        return sigbytes

    def sendSyn(self, signal, data):
        #print "Sending SYN"
        SYN = RIPMessage()
        SYN.sequence_number_notification_flag = True
        SYN.sequence_number = InitCSeqNum
        n = os.urandom(8).encode('hex')
        c = CertFactory.getCertsForAddr(self.transport.getHost().host)
        SYN.certificate = [str(n), c[0], c[1]]
        SYN.signature = self.PacketSigner(SYN)
        self.transport.write(SYN.__serialize__())
        self.cb.Nonce = n

    def sendSynAck(self, signal, gotData):
        if self.IntegrityCheck(gotData, gotData.certificate[1]):
            #print "Received SYN with sequence number:", gotData.sequence_number, "\nSending SYNACK"
            self.cb.NextAcknowledgementNumber = gotData.sequence_number + 1
            self.cb.scert = gotData.certificate[2]
            self.cb.bcert = gotData.certificate[1]
            n = gotData.certificate[0]
            n1 = os.urandom(8).encode('hex')
            self.cb.sessionID = str(n1)+str(n)
            IntN = int(n, 16)
            IntN = IntN+1
            n = self.intToNonce(IntN)
            SYNACK = RIPMessage()
            c = CertFactory.getCertsForAddr(self.transport.getHost().host)
            SYNACK.certificate = [n1, n, c[0], c[1]]
            SYNACK.sequence_number = InitSSeqNum
            SYNACK.acknowledgement_number = self.cb.NextAcknowledgementNumber
            SYNACK.sequence_number_notification_flag = True
            SYNACK.acknowledgement_flag = True
            self.cb.Nonce = n1
            self.cb.NextAcknowledgementNumber +=1
            SYNACK.signature = self.PacketSigner(SYNACK)
            self.transport.write(SYNACK.__serialize__())
        else:
            print "oops"

    def sendAck(self, signal, gotData):
        if (signal!="RESET"):
            if self.IntegrityCheck(gotData, gotData.certificate[2]):
                if (self.verifier(gotData.certificate[3], gotData.certificate[2], gotData.certificate[1])):
                    print "Received SYN-ACK with Sequence Number:", gotData.sequence_number, "and Acknowledgement Number:", gotData.acknowledgement_number, "\nSending ACK\nEstablished"
                    self.cb.NextSequenceNumber = gotData.acknowledgement_number
                    self.cb.NextAcknowledgementNumber = gotData.sequence_number + 1
                    self.cb.scert = gotData.certificate[3]
                    self.cb.bcert = gotData.certificate[2]
                    b = gotData.certificate[0]
                    IntN = int(b, 16)
                    IntN = IntN+1
                    b = self.intToNonce(IntN)
                    ACK = RIPMessage()
                    ACK.sequence_number = self.cb.NextSequenceNumber
                    ACK.acknowledgement_number = self.cb.NextAcknowledgementNumber
                    ACK.acknowledgement_flag = True
                    ACK.certificate = [b]
                    self.cb.NextSequenceNumber+=1
                    self.cb.sessionID = str(self.cb.Nonce) + gotData.certificate[0]
                    ACK.sessionID = self.cb.sessionID
                    ACK.signature = self.PacketSigner(ACK)
                    self.transport.write(ACK.__serialize__())
                    self.makeHigherConnection(self.HigherTransport)
                else:
                    print "Bad Certificates!"
            else:
                print "Integrity"

    def sendFinAck(self, signal, gotData):
        if (self.IntegrityCheck(gotData, self.cb.bcert)):
            FIN = RIPMessage()
            #print "Received FIN ", gotData.sequence_number, gotData.acknowledgement_number
            FIN.sequence_number = self.cb.NextSequenceNumber
            FIN.acknowledgement_number = gotData.sequence_number+1
            FIN.sessionID = self.cb.sessionID
            FIN.close_flag = True
            FIN.acknowledgement_flag = True
            FIN.signature = self.PacketSigner(FIN)
            self.transport.write(FIN.__serialize__())
            self.higherProtocol().connectionLost(self.HigherTransport)
            self.fsm.signal("RESET", "")
        else:
            print "Error: Signature of Packet not Verified!"

    def ResendControlPacket(self, Packet, state):
        if self.fsm.currentState()==state:
            self.transport.write(Packet.__serialize__())

    def finAck(self, signal, data):
        if (signal!="RESET"):
            if(self.IntegrityCheck(data, self.cb.bcert)):
                #print "FINACK received",data.sequence_number, data.acknowledgement_number
                if self.factory.state == "server":
                    self.higherProtocol().connectionLost(self.HigherTransport)
                else:
                    #print "Connection Closed"
                    self.higherProtocol().connectionLost(self.HigherTransport)
            else:
                print "Error: Signature of Packet not Verified!"
        else:
            self.higherProtocol().connectionLost(self.HigherTransport)

    def waitFinack(self, signal, gotData):
        print "Closing Connection..\nFIN SENT"

    def Established(self, signal, gotData):
        if signal == "ACK":
            if self.IntegrityCheck(gotData, self.cb.bcert) and int(gotData.certificate[0], 16)==int(self.cb.Nonce, 16)+1:
                #print "Received ACK with sequence number:", gotData.sequence_number, "and Acknowledgement Number:", gotData.acknowledgement_number, "\nEstablished"
                self.cb.NextSequenceNumber = gotData.acknowledgement_number
                self.makeHigherConnection(self.HigherTransport)
            else:
                print "False"
        elif signal == "ACK1":
            if (self.IntegrityCheck(gotData, self.cb.bcert)):
                #print "Received ACK with sequence number:", gotData.sequence_number, "and Acknowledgement Number:", gotData.acknowledgement_number
                self.cb.NextSequenceNumber = gotData.acknowledgement_number
                #self.cb.NextAcknowledgementNumber = gotData.sequence_number
                i=0
                while(i<len(SentPackets)):
                    if (SentPackets[i][0]+len(SentPackets[i][1].data)+1==self.cb.NextSequenceNumber):
                        del SentPackets[0:i]
                        break
                    elif (len(SentPackets)==1):
                        del SentPackets[0]
                        break
                    i = i+1

            else:
                print "Error: Signature of Packet not Verified!"

    def Transmission(self, gotData):
        if(self.IntegrityCheck(gotData, self.cb.bcert)):
            if gotData.sequence_number == self.cb.NextAcknowledgementNumber:
                #print "Data Received in Order!", gotData.sequence_number, gotData.acknowledgement_number
                self.cb.NextAcknowledgementNumber = gotData.sequence_number + len(gotData.data)
                ACK = RIPMessage()
                ACK.sequence_number = self.cb.NextSequenceNumber
                ACK.acknowledgement_number = gotData.sequence_number + len(gotData.data)
                ACK.acknowledgement_flag = True
                ACK.sessionID = self.cb.sessionID
                ACK.signature = self.PacketSigner(ACK)
                self.transport.write(ACK.__serialize__())
                self.higherProtocol().dataReceived(gotData.data)
                if RecvdPackets:
                    for Packet in RecvdPackets:
                        self.cb.NextAcknowledgementNumber = Packet[1].sequence_number + len(Packet[1].data)
                        ACK = RIPMessage()
                        ACK.sequence_number = self.cb.NextSequenceNumber
                        ACK.acknowledgement_number = Packet[1].sequence_number + len(Packet[1].data)
                        ACK.acknowledgement_flag = True
                        ACK.sessionID = self.cb.sessionID
                        ACK.signature = self.PacketSigner(ACK)
                        self.transport.write(ACK.__serialize__())
                        self.higherProtocol().dataReceived(Packet[1].data)
            elif (gotData.sequence_number, gotData) not in RecvdPackets and gotData.sequence_number > self.cb.NextAcknowledgementNumber:
                if len(RecvdPackets)==0:
                    print "Out of Order!"
                RecvdPackets.append((gotData.sequence_number,gotData))
                RecvdPackets.sort()
                
                
        else:
            print "Error: Signature of Packet not Verified!"

    def Retransmission(self, test, ctr):
        flag = False
        if SentPackets and (set(test) <= set(SentPackets) or set(test)>=set(SentPackets)):
            for packet in test:
                self.transport.write(packet[1].__serialize__())
            ctr= ctr+1
            if (ctr==5):
                flag=True
            if not flag:
                task.deferLater(reactor, 4, self.Retransmission, test, ctr)

    def Listening(self, signal, gotData):

        del RecvdPackets[:]
        del SentPackets[:]
        self.cb.__init__()


class RIPClientFactory(Factory, StackingFactoryMixin):
    state = "client"
    protocol = RIPProtocol

class RIPServerFactory(Factory, StackingFactoryMixin):
    state = "server"
    protocol = RIPProtocol

ConnectFactory = RIPClientFactory()
ListenFactory = RIPServerFactory()
